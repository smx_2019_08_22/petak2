/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.sessionBeans;

import entities.Ocene;
import entities.Ucenici;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Grupa1
 */
@Stateless
public class OceneSessionBean implements OceneSessionBeanLocal {

    @PersistenceContext(unitName="SMSPU")
    private EntityManager em;
    
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    @Override
    public List<Ocene> getOceneUcenika(Integer ucenik) {
        try{
        Query findUcenik = em.createNamedQuery("Ucenici.findById");
        findUcenik.setParameter("id", ucenik);
        Ucenici mucenici = (Ucenici) findUcenik.getSingleResult();
        Query query = em.createNamedQuery("Ocene.findByUcenik");
        query.setParameter("ucenik", mucenici);
        List<Ocene> ocene = (List<Ocene>) query.getResultList();
        return ocene;
        }catch(NoResultException nre){
            return null;
        }catch(Exception e){
            e.printStackTrace();
            return null;
        } 
    }

    
}
