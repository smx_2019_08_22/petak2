package managedBeans;
import business.sessionBeans.DepartmentSessionBeanLocal;
import entities.Odeljenja;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean(name = "directorMB", eager = true)
@SessionScoped
public class DirectorManagedBeans {
    @EJB
    private DepartmentSessionBeanLocal departmentBean; 
    
    public List<Odeljenja> getDepartments() {
        return departmentBean.getAllDepartments();
    }
}
