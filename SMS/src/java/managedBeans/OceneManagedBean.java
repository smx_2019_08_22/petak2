/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedBeans;

import business.sessionBeans.OceneSessionBeanLocal;
import entities.Ocene;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.inject.Named;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Grupa1
 */
@ManagedBean(name = "oceneMB", eager = true)
@SessionScoped
public class OceneManagedBean implements Serializable{

    @EJB
    private OceneSessionBeanLocal oceneBean; 
    
    private Integer ucenik;
    
    public OceneManagedBean() {
        
    }
    public List<Ocene> getOceneUcenika(){
        setUcenik(1);
        return oceneBean.getOceneUcenika(getUcenik());
    }

    /**
     * @return the ucenik
     */
    public Integer getUcenik() {
        return ucenik;
    }

    /**
     * @param ucenik the ucenik to set
     */
    public void setUcenik(Integer ucenik) {
        this.ucenik = ucenik;
    }
    
    
}
