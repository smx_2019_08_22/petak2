package managedBeans;

import business.sessionBeans.StudentSessionBeanLocal;
import entities.Ucenici;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean(name = "studentMB", eager = true)
@SessionScoped
public class StudentManagedBean implements Serializable{
    
    private Integer odeljenje;
    private Integer ucenik;
    
    @EJB
    private StudentSessionBeanLocal studentBean;
    
    public List<Ucenici> getAllStudents() {
        
        return studentBean.getAllStudents();
    }
        public List<Ucenici> getAllStudentsFromDepartment() {
            setOdeljenje(1);
        return studentBean.getStudentsFromDepartment(getOdeljenje());
    }

    /**
     * @return the odeljenje
     */
    public Integer getOdeljenje() {
        return odeljenje;
    }

    /**
     * @param odeljenje the odeljenje to set
     */
    public void setOdeljenje(Integer odeljenje) {
        this.odeljenje = odeljenje;
    }

    /**
     * @return the ucenik
     */
    public Integer getUcenik() {
        return ucenik;
    }

    /**
     * @param ucenik the ucenik to set
     */
    public void setUcenik(Integer ucenik) {
        this.ucenik = ucenik;
    }
}
